<?php

namespace App\Models;

use App\Models\Mixins\{
    ForeverModel as Model,
    ActiveScope,
    Sluggable
};

class Sport extends Model
{
    use ActiveScope, Sluggable;

    # cast/extract json db fields:
    protected $casts = [
        'levels' => 'array',
        'states' => 'array',
        'genders' => 'array',
        'options' => 'array',
    ];

    # inverse of 'fillable' (not mass assignable):
    protected $guarded = [
        'id', 'legacy_id'
    ];

    # hidden from api responses:
    protected $hidden = [
        'id', 'legacy_id',
        'sort', 'slug',
        'active', #'options'
        'created_at', 'updated_at',
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationship(s)
    |--------------------------------------------------------------------------
    */

    // public function stories(): HasMany
    // {
    //     return $this->hasMany(Story::class);
    // }
}
