<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Mixins\{
    Sluggable,
    StoryAttributes
};

class Story extends Model
{
    use Sluggable, StoryAttributes;

    # cast/extract json db fields:
    protected $casts = [
        'tags' => 'array',
        'legacy_metadata' => 'array',
    ];

    # inverse of 'fillable' (not mass assignable):
    protected $guarded = [
        'id', 'legacy_id'
    ];

    # hidden from api responses:
    protected $hidden = [
        'id',
        'author_static',
        'publication_id',
        'location', 'location_static',
        'intro1', 'intro2',
        'headline1', 'headline2', 'headline3',
        'legacy_id', 'legacy_ownerid', 'legacy_type',
        'legacy_data', 'legacy_metadata',
        'legacy_active', 'forever_active',
        'slug', 'slugged_headline',
        'created_at', 'updated_at',
    ];

    #protected $with = ['publication'];

    /*
    |--------------------------------------------------------------------------
    | Relationship(s)
    |--------------------------------------------------------------------------
    */

    # sport

    # publication

    /*
    |--------------------------------------------------------------------------
    | Query Scope(s)
    |--------------------------------------------------------------------------
    */

    public function scopeActive($queryBuilder)
    {
        return $queryBuilder->where('forever_active', true);
    }

    /**
     * Query Scope (or facet): Season
     */
    public function scopeBySeason($queryBuilder, $year)
    {
        return ($year)
            ? $queryBuilder->where('year', $year)
            : $queryBuilder
        ;
    }

    public function scopeTagged($queryBuilder, $topic)
    {
        return ($topic)
            ? $queryBuilder->whereJsonContains('tags', $topic)
            : $queryBuilder
        ;
    }
}
