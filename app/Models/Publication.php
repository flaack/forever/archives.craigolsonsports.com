<?php

namespace App\Models;

use App\Models\Mixins\{
    ForeverModel as Model,
    ActiveScope,
    Sluggable
};

class Publication extends Model
{
    use ActiveScope, Sluggable;

    protected $guarded = ['id', 'legacy_ownerid'];

    protected $hidden = [
        'id',
        'legacy_ownerid',
        'legacy_name',
        'active',
        'sort',
        'scopes',
        'slugged',
        'settings',
        'republishable',
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationship(s)
    |--------------------------------------------------------------------------
    */

    public function stories()
    {
        return $this->hasMany(Story::class,
            # foreign key: stories.legacy_ownerid
            'legacy_ownerid',
            # local key: publications.legacy_ownerid
            'legacy_ownerid'
        );
    }

    public function fiveStories()
    {
        return $this->stories()->take(5);
    }

    public function authors()
    {
        // todo
    }


    /*
    |--------------------------------------------------------------------------
    | Query scope(s)
    |--------------------------------------------------------------------------
    */

    public function scopeRepublishable($queryBuilder)
    {
        $env_publications = config('app.publications');

        return $queryBuilder
            #->where('republishable', '=', 1)
            ->whereIn('id', $env_publications)
        ;
    }
}
