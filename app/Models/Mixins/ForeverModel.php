<?php

namespace App\Models\Mixins;

use Illuminate\Database\Eloquent\Model;

class ForeverModel extends Model {
    public $timestamps = false;
}
