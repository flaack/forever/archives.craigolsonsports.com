<?php

namespace App\Models\Mixins;

use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Accessors / mutators for the Story DB model
|--------------------------------------------------------------------------
*/
trait StoryAttributes
{
    /**
     * $story->active
     */
    public function getActiveAttribute()
    {
        return $this->forever_active;
    }

    /**
     * $story->author
     */
    public function getAuthorAttribute($value)
    {
        return $this->author_static;
    }

    /**
     * $story->author_static
     */
    public function getAuthorStaticAttribute($value)
    {
        return $value == ''
            ? '(Uncredited)'
            : $value
        ;
    }

    /**
     * $story->bookmarked (#TODO: not implemented)
     */
    public function getBookmarkedAttribute()
    {
        return false;
    }

    /**
     * $story->headline_sanitized
     */
    public function getHeadlineSanitizedAttribute()
    {
        return strip_tags(
            str_replace('<br>', ' – ', // ' 💫 ', # hall-of-fame icon: '<i class="material-icons">stars</i>',
                str_replace('<apos>', "'",
                    str_replace('<quot>', '"',
                        $this->headline
                    )
                )
            ),
            '<i>'
        );
    }

    /**
     * $story->byline
     */
    public function getBylineAttribute()
    {
        return ($this->headline == $this->headline2)
            ? ''
            : $this->headline2
        ;
    }

    /**
     * $story->placeline
     */
    public function getPlacelineAttribute()
    {
        return $this->location_static;
    }

    /**
     * $story->location_static
     */
    public function getLocationStaticAttribute($value)
    {
        return $value == ''
            ? 'Unknown (Fergus Falls?)'
            : $value
        ;
    }

    /**
     * $story->published->at
     * $story->published->ago
     * $story->published->month
     */
    public function getPublishedAttribute()
    {
        $weekDay = $this->created_at->format('l');
        $pubDate = $this->created_at->toFormattedDateString();
        $monthYY = $this->created_at->format('F Y');
        $longAgo = $this->created_at->diffForHumans();
        //
        return (object) [
            'at' => "$weekDay $pubDate",
            'ago' => $longAgo,
            'month' => $monthYY,
        ];

    }

    /**
     * $story->teaser
     */
    public function getTeaserAttribute()
    {
        return $this->intro1;
    }

    public function getStoryAttribute($story)
    {
        return trim($story);
    }
}
