<?php

namespace App\Models\Mixins;

use Illuminate\Support\Str;

trait Sluggable
{
    # protected $appends = ['route'];
    protected function getArrayableAppends()
    {
        $this->appends = array_unique(
            array_merge(
                $this->appends, ['route']
            )
        );

        return parent::getArrayableAppends();
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getRouteAttribute()
    {
        $class = class_basename(__CLASS__);
        // $class = self::class;
        // $class = (new \ReflectionClass($this))->getShortName();

        $pluralized_lowercase_model = Str::plural(Str::lower($class));

        return $pluralized_lowercase_model == 'stories'
            // alias 'news' to 'stories' - RouteServiceProvider isn't helping here I guess
            ? route('news.show', $this)
            // all other cases should work like this
            : route("{$pluralized_lowercase_model}.show", $this)
        ;

        // return [
        //     'web' => route("{$pluralized_lowercase_model}.show", $this),
        //     'api' => route("api.{$pluralized_lowercase_model}.show", $this/*, false*/),
        // ];
    }

    public function getSlugAttribute($value)
    {
        if (!$this->slugged) {
            $headline = $this->headline_sanitized;

            $hash = substr(sha1($this->id.$headline), 0, 9);
            $slug = Str::slug("{$headline}-{$hash}");

            $this->update([
                'slug' => $slug,
                'slugged_headline' => true,
            ]);

            return $slug;
        }
        else {
            return $value;
        }
    }
}
