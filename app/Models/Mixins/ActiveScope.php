<?php

namespace App\Models\Mixins;

use Illuminate\Database\Eloquent\Builder;

trait ActiveScope
{
    public function scopeActive(Builder $query): void
    {
        $query->where('active', '=', 1);
    }
}
