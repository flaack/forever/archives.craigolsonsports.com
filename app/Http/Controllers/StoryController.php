<?php

namespace App\Http\Controllers;

use App\Models\Story;
use Illuminate\Http\{Request, Response};
use Illuminate\Support\Facades\DB;

class StoryController extends Controller
{
    public $per_page = 20;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Array $facets = [])
    {
        $db_like_operator = config('database.default') == 'mysql'
            # mysql (localhost)
            ? 'like'
            # pgsql (supabase)
            : 'ilike'
        ;

        $per_page = $request->has('per_page')
            ? $request->per_page
            # default page size:
            : 50
        ;

        $season = $request->has('season')
            ? $request->season
            # default season to start on:
            : 2001
        ;

        $headline_search = $request->has('headline_search')
            ? $request->headline_search
            # default hard-coded search phrase:
            : 'lady spartans'
        ;

        $stories = Story::active()
            ->bySeason($season)
            ->where('headline', $db_like_operator, '%'.$headline_search.'%')
            ->paginate($per_page)
        ;

        return view('news.index', compact(
            'season', 'stories',
            'headline_search',
            'per_page'
        ));
    }

    # Php Annotations like:
    #
    # [Route(Http::GET, '/shuffle')]
    public function shuffleIndex(Request $request)
    {
        $per_page = $this->per_page;

        $fulldeck = range(1,60096);

        shuffle($fulldeck);

        $keys = array_slice($fulldeck, 0, $per_page);

        $randomStories = Story::active()
            ->whereIn('id', $keys)
            ->paginate($per_page)
        ;

        return view('shuffle.index', compact(
            'randomStories',
            'per_page'
        ));
    }

    public function taggedIndex(Request $request, String $topic)
    {
        $per_page = $this->per_page;

        $taggedStories = Story::active()
            ->tagged($topic)
            ->paginate($per_page)
        ;

        return view('news.tagged', compact(
            'topic', 'taggedStories',
            'per_page'
        ));
    }

    /**
     * Display the specified resource.
     */
    public function show(Story $story)
    {
        return view('news.show', compact('story'));
    }
}
