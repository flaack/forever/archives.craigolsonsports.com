<?php

namespace App\Http\Controllers;

use App\Models\Publication;
use Illuminate\Http\Request;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $publications = Publication::republishable()->get();

        return view('publications.index', compact('publications'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Publication $publication)
    {
        return $publication;
    }
}
