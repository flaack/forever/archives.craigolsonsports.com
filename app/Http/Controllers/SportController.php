<?php

namespace App\Http\Controllers;

use App\Models\Sport;
use Illuminate\Http\Request;

class SportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sports = Sport::active()
            ->orderBy('sort')
            ->get()
        ;

        return view('sports.index', compact('sports'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Sport $sport)
    {
        // if (request is api:) return $sport api resource;
        return view('sports.show', compact('sport'));
    }
}
