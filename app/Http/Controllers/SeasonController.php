<?php

namespace App\Http\Controllers;

use App\Models\Season;
use Illuminate\Http\Request;

class SeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $seasons = range(1999,2020);

        return view('seasons.index', compact('seasons'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Season $season)
    {
        return view('seasons.show', compact('season'));
    }
}
