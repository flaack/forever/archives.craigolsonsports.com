<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PublicationSeeder extends Seeder
{
    public $table = 'publications';

    /**
     * placeholder array / json for future-flexible scoping
     */
    private $scopes = [
        ['active_period' =>
            [
                'begin_date' => 'pub_begin_date',
                'end_date'   => 'pub_end_date',
            ],
        ],
    ];

    /**
     * Original Publications (regional websites)
     */
    private $publications = [
        [
            'legacy_ownerid'=> 1,
            'legacy_name'   => 'PrepGIANT',
            'name'          => 'PrepGIANT',
            'site'          => 'http://www.prepgiant.com', # 'http://www.minnesota-scores.net/',
            'sort'          => 0,
            'slug'          => '0be965e8bf20da2b205ddd026903a4e3',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Ryan Phelps',
                'legacy_db'      => 'mwspn_main'
            ],
        ],
        [
            'legacy_ownerid'=> 2,
            'legacy_name'   => 'CraigOlsonSports.com',
            'name'          => 'Craig Olson Sports',
            'site'          => 'https://www.craigolsonsports.com/',
            'sort'          => 1,
            'slug'          => 'ace57d73603c0e361279dc9695dde9d2',
            'slugged'       => 0,
            'active'        => 1,
            'republishable' => 1,
            'settings'      => [
                'legacy_contact' => 'Craig Olson',
                'legacy_db'      => 'mwspn_cosports',
            ],
        ],
        [
            'legacy_ownerid'=> 3,
            'legacy_name'   => 'SportsCow.com',
            'name'          => 'Sports Cow',
            'site'          => 'http://www.sportscow.com/',
            'sort'          => 2,
            'slug'          => '742ac5ad3a3ba9490f495daf540bdbce',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 1,
            'settings'      => [
                'legacy_contact' => 'Kyle Gylsen',
                'legacy_db'      => 'mwspn_sportscow',
            ],
        ],
        [
            'legacy_ownerid'=> 4,
            'legacy_name'   => 'PheasantCountrySports.com',
            'name'          => 'Pheasant Country Sports',
            'site'          => 'http://www.pheasantcountrysports.com/',
            'sort'          => 3,
            'slug'          => '4aea84490f1b422b01ccdf809ad2f916',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 1,
            'settings'      => [
                'legacy_contact' => 'Kyle Gylsen',
                'legacy_db'      => 'mwspn_pcsports',
            ],
        ],
        [
            'legacy_ownerid'=> 5,
            'legacy_name'   => 'FM GameTime.com',
            'name'          => 'FM GameTime.com',
            'site'          => 'http://www.minnesota-scores.net/',
            'sort'          => 4,
            'slug'          => '826f912e1ddbb218643a2ea247656a8a',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Brent Rogness',
                'legacy_db'      => 'mwspn_fmgametime',
            ],
        ],
        [
            'legacy_ownerid'=> 6,
            'legacy_name'   => 'LakesAreaSports.com',
            'name'          => 'Lakes Area Sports',
            'site'          => 'https://www.lakesareasports.com/',
            'sort'          => 5,
            'slug'          => '1edbecb54a16d04011df81c7119208e5',
            'slugged'       => 0,
            'active'        => 1,
            'republishable' => 1,
            'settings'      => [
                'legacy_contact' => 'John George',
                'legacy_db'      => 'mwspn_lakesports',
            ],
        ],
        [
            'legacy_ownerid'=> 7,
            'legacy_name'   => 'Minnesota-Scores.net',
            'name'          => 'Minnesota Scores',
            'site'          => 'https://www.minnesota-scores.net/',
            'sort'          => 6,
            'slug'          => 'a9996e0ede41cefcc347e70107c5e45f',
            'slugged'       => 0,
            'active'        => 1,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Ryan Weinzierl',
                'legacy_db'      => 'mwspn_mnscores',
            ],
        ],
        [
            'legacy_ownerid'=> 8,
            'legacy_name'   => 'StateTourney.com',
            'name'          => 'StateTourney.com',
            'site'          => 'http://www.statetourney.com/',
            'sort'          => 7,
            'slug'          => '4f3fc877ce4db17e628591eb4488d617',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Darren Rogness',
                'legacy_db'      => 'mwspn_statetourney',
            ],
        ],
        [
            'legacy_ownerid'=> 9,
            'legacy_name'   => 'Minnesota Sports Online (MNSO)',
            'name'          => 'Minnesota Sports Online',
            'site'          => 'http://net.minnesotasportsonline.com', # 'http://www.minnesota-scores.net/',
            'sort'          => 8,
            'slug'          => '83e1feb4248686268322d33fb609cdab',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Ryan Phelps',
                'legacy_db'      => 'mwspn_mnsportsonline',
            ],
        ],
        [
            'legacy_ownerid'=> 10,
            'legacy_name'   => 'VikingLandSports.com',
            'name'          => 'Vikingland Sports',
            'site'          => 'http://www.vikinglandsports.com/',
            'sort'          => 9,
            'slug'          => '37818b83301885c57a3d8cb410d6da06',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 1,
            'settings'      => [
                'legacy_contact' => 'Kyle Gylsen',
                'legacy_db'      => 'mwspn_vikingland',
            ],
        ],
        [
            'legacy_ownerid'=> 14,
            'legacy_name'   => 'Minnesota-Scores Forum',
            'name'          => 'Minnesota-Scores Forum',
            'site'          => 'http://forum.minnesota-scores.net/',
            'sort'          => 10,
            'slug'          => '2aba71826f51270c506466557f62aad5',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Scott Wienerzrl',
                'legacy_db'      => 'mwspn_mnscoresbb',
            ],
        ],
        [
            'legacy_ownerid'=> 15,
            'legacy_name'   => 'Rocky Preps',
            'name'          => 'Rocky Preps',
            'site'          => 'http://coloradoscores.com/',
            'sort'          => 11,
            'slug'          => '22bc835f19e206efd4ee76e64bccdaa9',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Jerry Howard',
                'legacy_db'      => 'mwspn_rockypreps',
            ],
        ],
        [
            'legacy_ownerid'=> 19,
            'legacy_name'   => 'Colorado Sports',
            'name'          => 'Colorado Sports',
            'site'          => 'http://coloradoscores.com/',
            'sort'          => 12,
            'slug'          => '66822eb7cf6a3016882545a00fdd97a9',
            'slugged'       => 0,
            'active'        => 0,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Kevin',
                'legacy_db'      => 'mwspn_coloradosports',
            ],
        ],
        [
            'legacy_ownerid'=> 21,
            'legacy_name'   => 'NorthDakota-Scores',
            'name'          => 'North Dakota Scores',
            'site'          => 'https://northdakota-scores.net',
            'sort'          => 13,
            'slug'          => '0d647e591f657c0431c6df40d27e534d',
            'slugged'       => 0,
            'active'        => 1,
            'republishable' => 0,
            'settings'      => [
                'legacy_contact' => 'Ryan Weinzerl',
                'legacy_db'      => 'mwspn_ndscores',
            ],
        ],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $table = DB::table($this->table);

        # clear
        $table->truncate();

        # fill (only if empty)
        if ($table->count() == 0) {
            foreach($this->publications as $pub) {
                $table->insert([
                    'sort'           => $pub['sort'],
                    'legacy_ownerid' => $pub['legacy_ownerid'],
                    'legacy_name'    => $pub['legacy_name'],
                    'name'           => $pub['name'],
                    'site'           => $pub['site'],
                    'slug'           => $pub['slug'],
                    'slugged'        => $pub['slugged'],
                    'active'         => $pub['active'],
                    'republishable'  => $pub['republishable'],
                    'settings'       => json_encode($pub['settings']),
                    'scopes'         => json_encode($this->scopes),
                ]);
            }
        }
    }
}
