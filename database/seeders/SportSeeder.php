<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class SportSeeder extends Seeder
{
    public $table = 'sports';

    /**
     * sport scoring & statistical options, etc
     */
    public $options = [
        'scoring' => ['precision' => '0'],
        'ranking' => ['quotient'  => 1 ],
        'standings' => [],
        'statistical' => [
            'athlete_grade'  => 'true',
            'athlete_height' => 'true',
            'athlete_weight' => 'false',
            'athlete_jersey' => 'true',
        ],
    ];

    /**
     * (High School) sports seeded by default
     */
    public $sports = [
        '100' => [
            'name'    => 'Baseball',
            'slug'    => 'baseball',
            'sort'    => 5,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['boys',],
        ],
        '101' => [
            'name'    => 'Basketball (B)',
            'slug'    => 'basketball-boys',
            'sort'    => 2,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['boys',],
        ],
        '102' => [
            'name'    => 'Basketball (G)',
            'slug'    => 'basketball-girls',
            'sort'    => 2,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['girls',],
        ],
        '104' => [
            'name'    => 'Football',
            'slug'    => 'football',
            'sort'    => 0,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['boys',],
        ],
        '112' => [
            'name'    => 'Soccer (B)',
            'slug'    => 'soccer-boys',
            'sort'    => 1,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['boys',],
        ],
        '109' => [
            'name'    => 'Hockey (B)',
            'slug'    => 'hockey-boys',
            'sort'    => 3,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['boys',],
        ],
        '110' => [
            'name'    => 'Hockey (G)',
            'slug'    => 'hockey-girls',
            'sort'    => 3,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['girls',],
        ],
        '113' => [
            'name'    => 'Soccer (G)',
            'slug'    => 'soccer-girls',
            'sort'    => 1,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['girls',],
        ],
        '114' => [
            'name'    => 'Softball',
            'slug'    => 'softball',
            'sort'    => 5,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['girls',],
        ],
        '120' => [
            'name'    => 'Volleyball',
            'slug'    => 'volleyball',
            'sort'    => 0,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['girls',],
        ],
        '121' => [
            'name'    => 'Wrestling',
            'slug'    => 'wrestling',
            'sort'    => 4,
            'levels'  => ['hs'],
            'states'  => ['MN','ND',],
            'genders' => ['boys',],
        ],
        '128' => [
            'name'    => 'Lacrosse (B)',
            'slug'    => 'lacrosse-boys',
            'sort'    => 6,
            'levels'  => ['hs'],
            'states'  => ['MN',],
            'genders' => ['boys',],
        ],
        '129' => [
            'name'    => 'Lacrosse (G)',
            'slug'    => 'lacrosse-girls',
            'sort'    => 6,
            'levels'  => ['hs'],
            'states'  => ['MN',],
            'genders' => ['girls',],
        ],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $table = DB::table($this->table);

        # clear
        $table->truncate();

        # fill (only if empty)
        if ($table->count() == 0) {
            foreach($this->sports as $legacy_id => $sport) {
                $table->insert([
                    'legacy_id' => $legacy_id,
                    'sort'      => $sport['sort'],
                    'name'      => $sport['name'],
                    'slug'      => $sport['slug'],
                    'levels'    => json_encode($sport['levels']),
                    'states'    => json_encode($sport['states']),
                    'genders'   => json_encode($sport['genders']),
                    'options'   => json_encode($this->options),
                    # 'active' defaults to true/Y/1
                    # no timestamps
                ]);
            }
        }
    }
}
