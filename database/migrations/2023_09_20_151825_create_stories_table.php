<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\{
    Migrations\Migration,
    Schema\Blueprint,
    Query\Expression
};


return new class extends Migration
{
    public $table = 'stories';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        # mysql defaults
        // $defaults = [
        //     'hash_unique_default' => new Expression('(MD5(RAND()))'),
        //     'json_object_default' => new Expression('(JSON_OBJECT())'),
        //     'json_array_default'  => new Expression('(JSON_ARRAY())'),
        // ];

        # pgsql defaults
        $defaults = [
            'hash_unique_default' => new Expression('(md5(concat(now(),random())))'),
            'json_object_default' => new Expression('(json_build_object())'),
            'json_array_default'  => new Expression('(json_build_array())'),
        ];

        $storiesSchema = function(Blueprint $table) use ($defaults) {
            $table->id();
            $table->integer('legacy_id');

            $table->integer('publication_id')->nullable();
            $table->integer('legacy_ownerid')->nullable();

            $table->string('author_static')->nullable();
            $table->string('location_static')->nullable();

            $table->string('headline', 120)->nullable();
            $table->string('byline', 100)->nullable();
            $table->string('subtitle', 100)->nullable();
            $table->text('intro1')->nullable();
            $table->text('intro2')->nullable();
            $table->longText('story')->nullable();

            $table->string('slug')->unique()
                ->default($defaults['hash_unique_default'])
            ;

            $table->boolean('slugged_headline')->default(false);

            $table->json('tags')->nullable()
                ->default($defaults['json_array_default'])
            ;

            $table->integer('legacy_type')->nullable();
            $table->boolean('legacy_active')->nullable();
            $table->boolean('forever_active')->nullable();

            # shove all other legacy attributes in here
            $table->json('legacy_metadata')->nullable()
                ->default($defaults['json_object_default'])
            ;

            $table->year('year');
            $table->timestamps();
        };

        Schema::create($this->table, $storiesSchema);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }
};
