<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public $table = 'publications';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->integer('sort')->nullable();
            $table->integer('legacy_ownerid')->nullable();
            $table->string('legacy_name')->nullable();
            $table->string('name')->nullable();
            $table->string('site')->nullable();
            $table->string('slug')->unique();
            $table->boolean('slugged')->default(false);
            $table->boolean('active')->default(true);
            $table->boolean('republishable')->default(false);
            $table->json('settings')->nullable();
            $table->json('scopes')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }
};
