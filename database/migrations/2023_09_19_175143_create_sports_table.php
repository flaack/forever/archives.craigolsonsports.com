<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public $table = 'sports';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->integer('sort')->nullable();

            $table->integer('legacy_id')->index();
            $table->string('name');
            $table->string('slug')->unique();

            $table->json('levels')->nullable();
            $table->json('states')->nullable();
            $table->json('genders')->nullable();
            $table->json('options')->nullable();

            $table->boolean('active') # Y/N
                ->default(1)
                ->index()
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }
};
