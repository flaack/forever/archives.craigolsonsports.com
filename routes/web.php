<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\RedirectResponse;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::resources([
    'news' => StoryController::class,
    'sports' => SportController::class,
    'seasons' => SeasonController::class,
    'publications' => PublicationController::class,
]);

Route::get('/',
    fn() => redirect(route('news.index'))
);

Route::get('/news/tagged/{topic}', [
    StoryController::class, 'taggedIndex'
]);

Route::get('/shuffle', [
    StoryController::class, 'shuffleIndex'
]);

Route::view('/chase', 'chase');
Route::view('/sirius', 'sirius');

Route::view('/experimental', 'journalism');

Route::get('/craig',
    # source: https://544foundation.org/distinguished-alumni/2004-hall-of-fame-inductees/
    fn() => '</p>With his arrival in Fergus Falls in 1981, broadcaster Craig Olson became the voice of Otter sports for thousands of local radio listeners. He averaged more than 100 sporting events per year, as well as hosting morning sports call-in shows and talk shows with coaches. Olson began his radio career at the age of 15 in his hometown of Detroit Lakes and worked in radio in Missouri and Utah before moving to Fergus Falls. He emceed countless athletic awards banquets and programs during his career in Fergus Falls. He was inducted into the Fergus Falls Community College Sports Hall of Fame in 1993 and the Fergus Falls Chamber of Commerce Sports Hall of Fame in 1998. Olson was also nominated as Minnesota Sportscaster of the Year.</p>'
);
