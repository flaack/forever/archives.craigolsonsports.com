@extends( '__layouts.archives' )

@section('content')
<body>
    <blockquote>🗞 🎙 📸 📰</blockquote>
    <div class="loader"></div>
    <p>Over sixty thousand (60,096 to be exact) sports stories, published over twenty years</p>
    <div class="loader"></div>
    <p>Craig Olson is a machine (and Craig.AI is actually a machine)</p>
    <div class="loader"></div>
    <p>
        <a href="#">📆 almanac (i.e. <em>on this day</em>)</a> | <a href="#">🎲 random</a> | <a href="#">🕵🏻‍♂️ find</a> or <a href="#">ask chatty</a> | <a href="#">🤖 autowrite <sup>( by Craig.AI )</sup></a>
    </p>
    <div class="loader"></div>
    <p>
        stats for nerds:
    </p>
    <div style="margin-left: 2rem">
        <p>charts and graphs:
            <ul>
                <li>stories per year (via day-by-day indicators)</li>
                <li>stories per author</li>
                <li>most viewed stories</li>
                <li>most mentioned athletes</li>
                <li>most mentioned teams</li>
                {{--
                    // see data-vis samples at
                    // https://2019.wattenberger.com/
                --}}
            </ul>
        </p>
    </div>
</body>
@endsection


{{--
incorporate this sport 'glossary'
https://tachyons.io/components/definition-lists/inline/index.html
 --}}
