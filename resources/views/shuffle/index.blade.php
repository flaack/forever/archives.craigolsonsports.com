@extends( '__layouts.archives' )

@section('content')
<h3>Just {{$per_page}} random stories...</h3>

<hr/>

<div class="content-grid">

    @forelse( $randomStories as $story )
        @php
            $gridClass = match($loop->index) {
                3, 7, 15 => 'breakout',
                9, 19 => 'full-width',
                default => 'standard',
            };
        @endphp
        <article class="hx-article {{$gridClass }}">
            <code style="background-color:silver;">.hx-article .{{$gridClass}} </code>
            <a href="{{ $story->route }}" title="{{ $story->headline_sanitized }}">
                <h4>{{ $story->headline_sanitized }}</h4>
            </a>
            <p>{!! $story->teaser !!}</p>
        </article>
    @empty
        <h4>
            (No random news, wtf? <a href="#">random topic</a>)
        </h4>
    @endforelse
</div>
@endsection
