@extends('__layouts.archives')

@section('content')

<div class="index">
    <header class="text-center">
        <h2>Browse by Originating Publication / Regional Website (same thing)</h2>
    </header>

    <section class="grid">
        @foreach($publications as $publication)
        <div class="publication card" style="border:1px solid; border-radius:6px;">
            <a href="{{ route('publications.show', $publication) }}">
                <h4>🗞 {{ $publication->name }}</h4>

                <p class="text-center">
                    Browse <strong>{{ number_format($publication->stories()->count()) }}</strong> stories
                    <br>
                    <span class="byline">
                        by <em>NN</em> contributing writers!
                    </span>
                    <br>
                    originally appearing on:
                    <cite class="blockquote-footer"><small>{{ $publication->site }}</small></cite>
                </p>
            </a>
        </div>
        @endforeach
    </section>
</div>

@endsection
