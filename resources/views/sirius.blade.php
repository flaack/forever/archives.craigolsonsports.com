@extends( '__layouts.archives' )

@section('content')
    <iframe width="100%" style="aspect-ratio:16/9; border:24px inset silver; margin:8px 0"
        src="https://www.youtube-nocookie.com/embed/H4CYgDBgEn4?si=zvxN8nQ0UkvF5Kv_"
        title="And now... the starting line-up..."
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowfullscreen
    >
    </iframe>
@endsection
