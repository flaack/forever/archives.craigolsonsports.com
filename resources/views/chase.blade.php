@extends( '__layouts.archives' )

@section('content')

    <iframe width="100%" style="aspect-ratio:16/9; border:24px inset silver; margin:8px 0"
        src="https://www.youtube-nocookie.com/embed/inspk_OTw5s?si=5B84M8k2pslaXY-H"
        title="Chase"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture;"
        allowfullscreen
    >
    </iframe>

    <hr/>

    {{-- <div style="color: #888; line-break: anywhere; word-break: normal; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">
        <a href="https://soundcloud.com/giorgiomoroder/chase" title="Giorgio Moroder - Chase [Midnight Express] (1978)" target="_blank" style="color: #888; text-decoration: none;">
            Giorgio Moroder - Chase [Midnight Express] (1978)
        </a>
    </div> --}}

    <iframe width="100%" height="300"  style="border:24px inset silver; margin:8px 0"
        scrolling="no"
        frameborder="no"
        allow="autoplay"
        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/129726439&color=%23ff5500&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
    >
    </iframe>

@endsection
