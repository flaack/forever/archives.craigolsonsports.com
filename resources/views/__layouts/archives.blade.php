<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-theme="light" class="light">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name', 'archives.CraigOlsonSports.com') }}</title>
        <link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🗞</text></svg>">
        <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic|Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css">
        @vite('resources/css/app.scss')
    </head>
    <!--//
    YOUR AREA SPORTS WRAP... SINCE 1999
    //-->
    <body>
        <nav class="container-fluid" class="logo-fx">
            <a href="/" aria-label="Back to the Archives Home">
                <ul class="logo-fx">
                    <li><div class="wayback-loader"></div></li>
                    <li><strong>archives.CraigOlsonSports.com</strong></li>
                </ul>
            </a>
            <ul>
                <li class="flag-off">
                    <details role="list" dir="rtl">
                        <summary aria-haspopup="listbox" role="link">Site</summary>
                        <ul role="listbox">
                            <li>
                                <label>
                                    <input type="checkbox">
                                    Craig Olson Sports
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="checkbox">
                                    Lakes Area Sports
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="checkbox">
                                    Sports Cow
                                </label>
                            </li>
                        </ul>
                    </details>
                </li>
                <li>
                    <details role="list" dir="rtl">
                        <summary aria-haspopup="listbox" role="link">Sports</summary>
                        <ul role="listbox">
                            <li>
                                <a href="{{ route('sports.index') }}">(go to sports index)</a>
                            </li>
                            {{-- <li><a>Football</a></li>
                            <li><a>Volleyball</a></li> --}}
                        </ul>
                    </details>
                </li>
                <li>
                    <details role="list" dir="rtl">
                        <summary aria-haspopup="listbox" role="link">Seasons</summary>
                        <ul role="listbox">
                            <li>
                                <a href="{{ route('seasons.index') }}">(go to seasons index)</a>
                            </li>
                            {{-- <li><a>1999</a></li>
                            <li><a>2000</a></li>
                            <li><a>2001</a></li> --}}
                        </ul>
                    </details>
                </li>
                <li class="flag-off">
                    <details role="list" dir="rtl">
                        <summary aria-haspopup="listbox" role="link">Schools</summary>
                        <ul role="listbox">
                            <li><a>FFHS</a></li>
                            <li><a>PLHS</a></li>
                            <li><a>DLHS</a></li>
                        </ul>
                    </details>
                </li>
                <li class="flag-off">
                    <a href="#" role="button">Sign in</a>
                </li>
            </ul>
        </nav>

        <!-- <header>~ Header</header> -->

        <main role="main" class="container">
            <div class="content container-xl">
                @yield('content')
            </div>
        </main>

        {{-- @include('footer') --}}

                <footer class="pico-override">
            <div class="impressum">Over sixty thousand (60,096 to be exact) sports stories, spanning over two decades</div>
            <span class="sports-wrap">
                🗞 &nbsp;YOUR AREA SPORTS WRAP... SINCE 1999&nbsp; 🗞
            </span>
            <div>&copy; 1999–2023 Craig Olson and CraigOlsonSports.com</div>
        </footer>

{{--         <footer class="footer fixed-bottom bg-dark mt-auto py-2">
          <div class="container small text-center text-light font-weight-lighter">
            Archives Rebooted. Copyright &copy; 1995&ndash;{{ date('Y') }} CraigOlsonSports.com in partnership with Flaack.xyz
            &bull;
            <a href="#" class="text-decoration-none">Terms</a>
            &bull;
            <a href="#" class="text-decoration-none">Legal</a>
            &bull;
            <a href="#" class="text-decoration-none">Privacy</a>
          </div>
        </footer> --}}



    </body>
</html>

<script>
console.log(
    " %c YOUR AREA SPORTS WRAP... SINCE 1999   ",
    "background-color: #555 ; color: white ; font-weight: bold ; " +
    "font-size: 22px ; font-style: italic ; " +
    "font-family: 'american typewriter' ; text-shadow: 1px 1px 3px black ;"
);
</script>
