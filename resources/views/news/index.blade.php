@extends( '__layouts.archives' )

@section('content')
<h3>Browsing first <u>{{$per_page}}</u> stories published in the year <em>{{ $season }}</em></h3>
<h4>.. with headlines matching the phrase: <em>{{ $headline_search }}</em></h4>

<hr/>

<div>
    <ul>
    @forelse( $stories as $story )
        <li>
            {{-- <a href="{{ $story->route }}" style="display:block"> --}}
            <a href="{{ $story->route }}">
                {{ $story->headline_sanitized }}
            </a>
          {{-- ( {{ $story->schools()->count() }} mentioned schools ) --}}
          {{-- @foreach( $story->schools as $school )
            <sub style="font-size:.8rem">
              &nbsp;&bull;
              <a href="{{ $school->route }}">{{ $school->code }}</a>
            </sub>
          @endforeach --}}
      </li>
    @empty
        <h4>
            (No news)
            {{-- <x-random-story /> --}}
        </h4>
    @endforelse
    </ul>
</div>
@endsection
