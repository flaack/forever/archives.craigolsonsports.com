@extends( '__layouts.archives' )

@section('content')
<h3>Browsing first {{$per_page}} stories tagged <span class="tagged-topic">{{ $topic }}</span></h3>

<hr/>

<div>
    <ul>
    @forelse( $taggedStories as $story )
        <li>
            {{-- <a href="{{ $story->route }}" style="display:block"> --}}
            <a href="{{ $story->route }}">
                {{ $story->headline_sanitized }}
            </a>
            @if (count($story->tags) > 1)
            <dl>
                <dd>
                    <sup>Also tagged: {{ collect(array_diff($story->tags, [$topic]))->flatten()->join(', ') }}</sup>
                </dd>
            </dl>
            @endif
          {{-- ( {{ $story->schools()->count() }} mentioned schools ) --}}
          {{-- @foreach( $story->schools as $school )
            <sub style="font-size:.8rem">
              &nbsp;&bull;
              <a href="{{ $school->route }}">{{ $school->code }}</a>
            </sub>
          @endforeach --}}
      </li>
    @empty
        <h4>
            (No tagged news, wtf? <a href="#">random topic</a>)
            {{-- <x-random-story /> --}}
        </h4>
    @endforelse
    </ul>
</div>
@endsection
