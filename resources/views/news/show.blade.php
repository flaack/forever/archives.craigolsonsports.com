@extends( '__layouts.archives' )

@section('content')
<section class="story">
    <article class="klass">
        <h2>{{ $story->headline }}</h2>

        <h4>
            by {{ $story->author }}
            <br/>
            <time>{{ $story->published->at }} ({{ $story->published->ago }})</time>
        </h4>

        <p class="story">{!! $story->story !!}</p>

        <hr/>
        <span style="float:right">
            Archives Topic Explorer:
            @foreach($story->tags as $topic)
                <a class="tagged-topic" href="/news/tagged/{{ $topic }}" title="news tagged {{ $topic }}">{{ $topic }}</a>
            @endforeach
        </span>
    </article>
</section>

<script>
    (document.addEventListener('click', function(event) {
        event.preventDefault();

        document
            .getElementsByTagName('article')[0]
            .classList.toggle('with-fx')
    }))
</script>
@endsection
