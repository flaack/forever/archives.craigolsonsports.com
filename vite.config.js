import { defineConfig, loadEnv } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig( ({command, mode}) => {

    // Load env file based on `mode` in the current working directory.
    // Set the third parameter to '' to load all env regardless of the `VITE_` prefix.
    const env = loadEnv(mode, process.cwd(), '')
    console.log(env.VITE_DOMAIN)

    return {
        // vite config
        define: {
            // __SITE_DOMAIN__: JSON.stringify(env.VITE_DOMAIN),
        },
        plugins: [
            laravel({
                input: [
                    'resources/css/app.scss',
                    'resources/js/app.js'
                ],
                detectTls: env.VITE_DOMAIN,
                refresh: true,
            }),
        ],
    }
})
